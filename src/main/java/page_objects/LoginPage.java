package page_objects;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import static com.codeborne.selenide.Selenide.*;
import static org.junit.Assert.assertTrue;


public class LoginPage {
    static CommonElements commonElements = new CommonElements();
    static TempBullshitClassWithXpath tempBullShitClassWithXpath = new TempBullshitClassWithXpath();


    public static WebElement getTitle(String Platform) {
        if (Platform.equals("Android")) {
            return $(By.xpath(tempBullShitClassWithXpath.androidDropDownSecondary)).shouldBe(Condition.visible);
        }
        return $(By.xpath(tempBullShitClassWithXpath.iosDropDownThird)).shouldBe(Condition.visible);

    }

    public static WebElement getProceednButton(String Platform) {

        if (Platform.equals("Android")) {
            return $(By.xpath(tempBullShitClassWithXpath.LoginPhoneInput)).shouldBe(Condition.visible);
        }
        return $(By.xpath(tempBullShitClassWithXpath.iosSelectRegCountry)).shouldBe(Condition.visible);

    }
    public static WebElement getEmailField(String Platform) {

        if (Platform.equals("Android")) {
            return $(By.xpath(tempBullShitClassWithXpath.LoginPhoneInput)).shouldBe(Condition.visible);
        }
        return $(By.xpath(tempBullShitClassWithXpath.iosSelectRegCountry)).shouldBe(Condition.visible);

    }
    public static WebElement getFirstName(String Platform) {
        return $(By.id(commonElements.appPrefix + ":id/editTextFirstNameSignIn")).shouldBe(Condition.visible);
    }
    public static WebElement getLastName() {

        return $(By.id(commonElements.appPrefix + ":id/editTextLastNameSignIn")).shouldBe(Condition.visible);
    }
    public static WebElement getSetPassword() {
        return $(By.id(commonElements.appPrefix + ":id/editTextFirstPassSignIn")).shouldBe(Condition.visible);
    }
    public static WebElement getSetConfPassword() {
        return $(By.id(commonElements.appPrefix + ":id/editTextSecondPassSignIn")).shouldBe(Condition.visible);
    }
    public static WebElement getSignUpBtn() {
        return $(By.id(commonElements.appPrefix + ":id/btnSignUp"));
    }
    public static WebElement getLogPass() {
        return $(By.id(commonElements.appPrefix + ":id/editTextPassword"));
    }
    public static WebElement getBtnLogin() {
        return $(By.id(commonElements.appPrefix + ":id/btnLogin"));
    }


}
